local_pgm := $(subdirectory)/crc_main
local_src := $(addprefix $(subdirectory)/, crc_main.c)
local_objs := $(subst .c,.o, $(local_src))

local_lib := 
local_lib_src := 

#$(eval $(call make-library, $(subdirectory)/$(local_lib) , $(local_lib_src)))

programs += $(local_pgm)
sources += $(local_src)

$(local_pgm): $(local_objs) $(libraries)
