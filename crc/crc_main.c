/**
 * @file crc_main
 * @Author IHAR
 * @date
 * @brief
 * 
 * Copyright (c) 2013 by IHAR
*/

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <stdint.h>

#define BUFF_SIZE			(4096)
#define FNAME_INPUT_DEFAULT		"system.img"
#define FNAME_OUTPUT_DEFAULT		"system_ok.img"
#define FNAME_CSUM_DEFAULT		"csum.txt"

typedef struct __csums
{
	int64_t val_even;
	int64_t val_odd;
	int byte_even;
	int byte_odd;
} csums_t;

/**
 * @name    calc_input_image_csum
 * @brief Calculates checksums for input image   
 *
 * @param [in]
 * @param [out]

 * @retval    
 * @retval   
 *
 * Example Usage:
 * @code
 * @endcode
 */
static int calc_input_image_csum(char* file, int64_t *b1, int64_t *b2)
{
	uint8_t buff[BUFF_SIZE];
	FILE* pFile = NULL;
	size_t result;
	int ret = 0;
	int i;

	*b1 = 0;
	*b2 = 0;

	pFile = fopen(file, "r");
	if(NULL == pFile)
		return -1;

	while (!feof(pFile)) {
		result = fread(buff, 1, BUFF_SIZE, pFile);
		if (ferror(pFile)) {
			ret = -1;
			goto exit;
		}
		for (i = 0; i < result; ++i) {
			if (i & 1)
				*b2 += buff[i];
			else
				*b1 += buff[i];
		}
	}

	fclose(pFile);
	return 0;

exit:
	fclose(pFile);
	return ret;
}

/**
 * @name calc_original_image_csum
 * @brief Calculates checksums for input image   
 *
 * @param [in]
 * @param [out]

 * @retval    
 * @retval   
 *
 * Example Usage:
 * @code
 * @endcode
 */
static int calc_original_image_csum(char* file, csums_t *csums)
{
	uint8_t buff[BUFF_SIZE];
	FILE* pFile = NULL;
	size_t result;
	int ret = 0;
	int i;
	csums->val_even = 0; 
	csums->val_odd = 0;

	pFile = fopen(file, "r");
	if(NULL == pFile)
		return -1;

	while (!feof(pFile)) {
		result = fread(buff, 1, BUFF_SIZE, pFile);
		if (ferror(pFile)) {
			ret = -1;
			goto exit;
		}
		for (i = 0; i < result; ++i) {
			if (i & 1)
				csums->val_odd += buff[i];
			else
				csums->val_even += buff[i];
		}
	}
	
	if ((result - 4) & 1) {
		csums->val_odd -= buff[result - 4];
		csums->val_even -= buff[result - 3];
	}
	else {
		csums->val_even -= buff[result - 4];
		csums->val_odd -= buff[result - 3];
	}

	csums->byte_even = buff[result - 4]; 
	csums->byte_odd = buff[result - 3]; 


	fclose(pFile);
	return 0;

exit:
	fclose(pFile);
	return ret;
}

static void inc_byte(int *b1, int *b2)
{
	(*b1)++;
	if (*b1 > 255) {
		*b1 -= 256;
		inc_byte(b2, b1);
	}
}

static void dec_byte(int *b1, int *b2)
{
	(*b1)--;
	if (*b1 < 0) {
		*b1 += 256;
		dec_byte(b2, b1);
	}
}

/**
 * @name    calc_output_crc
 * @brief Calculates CRC	   
 *
 * @param [in]
 * @param [out]

 * @retval    
 * @retval   
 *
 * Example Usage:
 * @code
 * @endcode
 */
static void calc_output_crc(int64_t b1, int64_t b2, uint8_t *b_even, uint8_t *b_odd, csums_t *csums)
{
	int64_t val_even = csums->val_even;
	int64_t val_odd = csums->val_odd;
	int byte_even = csums->byte_even;
	int byte_odd = csums->byte_odd;
	uint64_t p;

	if (b1 > val_even) {
		p = val_even;
		while (p <= b1 - 1) {
			inc_byte(&byte_even, &byte_odd);
			++p;
		}
	}
	else {
		p = b1;
		while (p <= val_even - 1) {
			dec_byte(&byte_even, &byte_odd);
			++p;
		}
	}

	if (b2 > val_odd) {
		p = val_odd;
		while (p <= b2 - 1) {
			inc_byte(&byte_odd, &byte_even);
			++p;
		}
	}
	else {
		p = b2;
		while (p <= val_odd - 1) {
			dec_byte(&byte_odd, &byte_even);
			++p;
		}
	}

	*b_even = (uint8_t)(0xFF & byte_even);
	*b_odd = (uint8_t)(0xFF & byte_odd);
}

/**
 * @name    create_output_img
 * @brief  Creates output image with CRC 
 *
 * @param [in]
 * @param [out]

 * @retval    
 * @retval   
 *
 * Example Usage:
 * @code
 * @endcode
 */
static int create_output_img(char* file_in, char* file_out, uint8_t b_even, uint8_t b_odd)
{
	uint8_t buff[BUFF_SIZE];
	FILE* pFileIn = NULL; 
	FILE* pFileOut = NULL;
	size_t result;
	int ret = 0;

	pFileIn = fopen(file_in, "r");
	if(NULL == pFileIn) {
		printf("Can't open input image:%s\n", file_in);
		return -1;
	}

	pFileOut = fopen(file_out, "w");
	if(NULL == pFileOut) {
		printf("Can't create output image:%s\n", file_out);
		return -1;
	}

	while (!feof(pFileIn)) {
		result = fread(buff, 1, BUFF_SIZE, pFileIn);
		if (ferror(pFileIn)) {
			printf("Input image reading error\n");
			ret = -1;
			goto exit;
		}
		if (fwrite(buff, 1, result, pFileOut) != result) {
			printf("Output image writing error\n");
			ret = -1;
			goto exit;	
		}
	}

	fputc(b_even, pFileOut);	
	fputc(b_odd, pFileOut);
	fputc(0, pFileOut);	
	fputc(0, pFileOut);	

exit:
	fclose(pFileIn);
	fclose(pFileOut);
	return ret;
}

/**
 * @name    save_checksum
 * @brief  
 *
 * @param [in]
 * @param [out]

 * @retval    
 * @retval   
 *
 * Example Usage:
 * @code
 * @endcode
 */
static int save_checksum(char* file, csums_t *csums)
{
	FILE* pFileOut = NULL;
	size_t result;
	int ret = 0;

	pFileOut = fopen(file, "w");
	if(NULL == pFileOut) {
		printf("Can't create output file:%s\n", file);
		return -1;
	}

	fprintf(pFileOut, "0x%x 0x%llx 0x%x 0x%llx", 
			csums->byte_even, (long long unsigned int)csums->val_even, 
			csums->byte_odd, (long long unsigned int)csums->val_odd);

	fclose(pFileOut);

	return ret;
}

/**
 * @name    save_checksum
 * @brief  
 *
 * @param [in]
 * @param [out]

 * @retval    
 * @retval   
 *
 * Example Usage:
 * @code
 * @endcode
 */
static int load_checksum(char* file, csums_t *csums)
{
	FILE* pFileOut = NULL;
	size_t result;
	int ret = 0;

	pFileOut = fopen(file, "r");
	if(NULL == pFileOut) {
		printf("Can't open file:%s\n", file);
		return -1;
	}

	fscanf(pFileOut, "0x%x 0x%llx 0x%x 0x%llx", 
			(unsigned *)&(csums->byte_even), (long long unsigned int *)&(csums->val_even), 
			(unsigned *)&(csums->byte_odd), (long long unsigned int *)&(csums->val_odd));

	fclose(pFileOut);

	return ret;
}

int patch_image(int argc, char **argv)
{
	int ret = 0;

	char *fname_input;
	char *fname_output;
	char *fname_csum;
	int64_t b1, b2;
	uint8_t b_even, b_odd;
	csums_t csums;

	if ((argc > 2) && (argv[2] != "")) {
		fname_csum = argv[2];
	}
	else {
		fname_input = FNAME_CSUM_DEFAULT;
	}

	if ((argc > 3) && (argv[3] != "")) {
		fname_input = argv[3];
	}
	else {
		fname_input = FNAME_INPUT_DEFAULT;
	}

	if ((argc > 4) && (argv[4] != "")) {
		fname_output = argv[4];
	}
	else {
		fname_output = FNAME_OUTPUT_DEFAULT;
	}

	printf("Checksum file:%s\n", fname_csum);
	printf("Input image:%s\n", fname_input);
	printf("Output image:%s\n", fname_output);

	if (load_checksum(fname_csum, &csums)) {
		printf("Checksum file reading error!!!\n");
		ret = -1;
		goto exit;

	}

	printf("Original image checksums: even 0x%x 0x%llx odd 0x%x 0x%llx\n", 
			csums.byte_even, (long long unsigned int)csums.val_even, 
			csums.byte_odd, (long long unsigned int)csums.val_odd);

	printf("\nCalculating input image...\n");
	if (calc_input_image_csum(fname_input, &b1, &b2)) {
		printf("Input image reading error!!!");
		ret = -1;
		goto exit;
	}
	
	printf("Input file checksums even=0x%llx odd=0x%llx\n", (long long unsigned int)b1, (long long unsigned int)b2);

	printf("\nCalculating crc...\n");
	calc_output_crc(b1, b2, &b_even, &b_odd, &csums);
	printf("CRC even=0x%x odd=0x%x\n", b_even, b_odd);


	printf("\nCreating output image:%s...\n", fname_output);
	if (create_output_img(fname_input, fname_output, b_even, b_odd)) {
		printf("Output image creating error!!!\n");
		ret = -1;
	}
	else {
		printf("Output image was created successfully!!!\n");
	}

exit:
	return ret;
}

int create_checksum_file(int argc, char **argv)
{
	int ret = 0;

	char *fname_checksum;
	char *fname_input;
	int64_t b1, b2;
	uint8_t b_even, b_odd;
	csums_t csums;

	if ((argc > 2) && (argv[2] != "")) {
		fname_checksum = argv[2];
	}
	else {
		fname_checksum = FNAME_CSUM_DEFAULT;
	}

	if ((argc > 3) && (argv[3] != "")) {
		fname_input = argv[3];
	}
	else {
		fname_input = FNAME_INPUT_DEFAULT;
	}

	printf("Input image:%s\n", fname_input);
	printf("Output to:%s\n", fname_checksum);
	
	printf("\nCalculating input image...\n");
	if (calc_original_image_csum(fname_input, &csums)) {
		printf("Input image reading error!!!");
		ret = -1;
		goto exit;
	}

	printf("Original image checksums: even 0x%x 0x%llx odd 0x%x 0x%llx\n", 
			csums.byte_even, (long long unsigned int)csums.val_even, 
			csums.byte_odd, (long long unsigned int)csums.val_odd);

	if(save_checksum(fname_checksum, &csums)) {
		printf("Can't save checksums results in %s!!!\n", fname_checksum);
		ret = -1;
		goto exit;	
	} else {
		printf("Calculated checksum was saved!!!\n");
	}

exit:
	return ret;

}

int main(int argc, char **argv)
{
	int ret = 0;
	

	printf("////////////////////////////////////////////////////////////\n");
	printf("// Image CRC patcher for SeaLand SL-8268 car DVD by IHAR  //\n");
	printf("// based on DisaSoft sources				  //\n");
	printf("////////////////////////////////////////////////////////////\n");

	if (argc == 1) {
		goto err;
	}

	if (strcmp(argv[1], "-c") == 0) {
		ret = patch_image(argc, argv);	
	}
	else if (strcmp(argv[1], "-s") == 0) {
		ret = create_checksum_file(argc, argv);	
	} else {
		goto err;
	}

	goto exit;

err:
	printf("Usage:\n"); 
	printf("  crc patching: %s -c [CHECKSUM] [INPUT_IMG] [OUTPUT_IMG]\n", argv[0]);
	printf("  checksum calculation:  %s -s [CHECKSUM] [ORIGINAL_IMG]\n", argv[0]);

exit:
	exit(ret);
}
